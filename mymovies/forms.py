from django import forms
from django.contrib.auth.forms import AuthenticationForm

from .models import Movie, Serie, Anime

class MovieForm(forms.ModelForm):

    class Meta:
        model = Movie
        image = forms.ImageField('image')
        fields = ('title', 'genre', 'description','image', 'ranking')
        exclude = ['user',]

class SerieForm(forms.ModelForm):

    class Meta:
        model = Serie
        image = forms.ImageField('image')
        fields = ('title', 'genre', 'description','image', 'ranking')
        exclude = ['user',]
class AnimeForm(forms.ModelForm):

    class Meta:
        model = Anime
        image = forms.ImageField('image')
        fields = ('title', 'genre', 'description','image', 'ranking')
        exclude = ['user',]

# User authentication

class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30,
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password'}))
