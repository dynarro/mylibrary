from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^movie$', views.movie_list, name='movie_list'),    
    url(r'^movie/(?P<pk>\d+)/detail/$', views.movie_detail, name='movie_detail'),
    url(r'^movie/new/$', views.movie_new, name='movie_new'),
    url(r'^movie/(?P<pk>\d+)/edit/$', views.movie_edit, name='movie_edit'),
    url(r'^movie/(?P<pk>\d+)/delete/$', views.movie_delete, name='movie_delete'),
    url(r'^serie$', views.series_list, name='series_list'),
    url(r'^serie/(?P<pk>\d+)/detail/$', views.serie_detail, name="serie_detail"),
    url(r'^serie/new/$', views.serie_new, name='serie_new'),
    url(r'^serie/(?P<pk>\d+)/edit/$', views.serie_edit, name='serie_edit'),
    url(r'^serie/(?P<pk>\d+)/delete/$', views.serie_delete, name='serie_delete'),
    url(r'^anime$', views.anime_list, name='anime_list'),
    url(r'^anime/(?P<pk>\d+)/detail/$', views.anime_detail, name="anime_detail"),
    url(r'^anime/new/$', views.anime_new, name='anime_new'),
    url(r'^anime/(?P<pk>\d+)/edit/$', views.anime_edit, name='anime_edit'),
    url(r'^anime/(?P<pk>\d+)/delete/$', views.anime_delete, name='anime_delete'),
]