from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Movie, Serie, Anime
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files.storage import FileSystemStorage
from .forms import MovieForm, SerieForm, AnimeForm, LoginForm


@login_required(login_url="login/")
def home(request):
    return render(request, 'registration/mymovies/home_page.html',{})

# Movie views
@login_required(login_url="login/")
def movie_list(request):
    movies = Movie.objects.filter(user=request.user)
    return render(request, 'registration/mymovies/movie_list.html', {'movies': movies})

def movie_detail(request, pk):
    movie = get_object_or_404(Movie, pk=pk)
    return render(request, 'registration/mymovies/movie_detail.html', {'movie': movie})

def movie_new(request):
    if request.method == "POST":
        form = MovieForm(request.POST, request.FILES or None)
        if form.is_valid():
            movie = form.save(commit=False)
            movie.user = request.user
            movie.save()
            return redirect('movie_detail', pk=movie.pk)
    else:
        form = MovieForm()
    return render(request, 'registration/mymovies/movie_edit.html', {'form': form})

def movie_edit(request, pk):
    movie = get_object_or_404(Movie, pk=pk)
    if request.method == "POST":
        form = MovieForm(request.POST, request.FILES or None, instance=movie)
        if form.is_valid():
            movie = form.save(commit=False)
            movie.save()
            return redirect('movie_detail', pk=movie.pk)
    else:
        form = MovieForm(instance=movie)
    return render(request, 'registration/mymovies/movie_edit.html', {'form': form})

def movie_delete(request,pk):
    movie = get_object_or_404(Movie, pk=pk)
    delete = movie.delete()
    return render(request, 'registration/mymovies/movie_list.html', {'movie': movie})

# Series views
@login_required(login_url="login/")
def series_list(request):
    series = Serie.objects.filter(user=request.user)
    return render(request, 'registration/mymovies/series_list.html', {'series': series})

def serie_detail(request, pk):
    serie = get_object_or_404(Serie, pk=pk)
    return render(request, 'registration/mymovies/serie_detail.html', {'serie': serie})

def serie_new(request):
    if request.method == "POST":
        form = SerieForm(request.POST)
        if form.is_valid():
            serie = form.save(commit=False)
            serie.user = request.user
            serie.save()
            return redirect('serie_detail', pk=serie.pk)
    else:
        form = SerieForm()
    return render(request, 'registration/mymovies/serie_edit.html', {'form': form})

def serie_edit(request, pk):
    serie = get_object_or_404(Serie, pk=pk)
    if request.method == "POST":
        form = SerieForm(request.POST, instance=serie)
        if form.is_valid():
            serie = form.save(commit=False)
            serie.save()
            return redirect('serie_detail', pk=serie.pk)
    else:
        form = SerieForm(instance=serie)
    return render(request, 'registration/mymovies/serie_edit.html', {'form': form})

def serie_delete(request,pk):
    serie = get_object_or_404(Serie, pk=pk)
    delete = serie.delete()
    return render(request, 'registration/mymovies/series_list.html', {'serie': serie})

# Anime views
@login_required(login_url="login/")
def anime_list(request):
    animes = Anime.objects.filter(user=request.user)
    return render(request, 'registration/mymovies/anime_list.html', {'animes': animes})

def anime_detail(request, pk):
    anime = get_object_or_404(Anime, pk=pk)
    return render(request, 'registration/mymovies/anime_detail.html', {'anime': anime})

def anime_new(request):
    if request.method == "POST":
        form = AnimeForm(request.POST)
        if form.is_valid():
            anime = form.save(commit=False)
            anime.user = request.user
            anime.save()
            return redirect('anime_detail', pk=anime.pk)
    else:
        form = AnimeForm()
    return render(request, 'registration/mymovies/anime_edit.html', {'form': form})

def anime_edit(request, pk):
    anime = get_object_or_404(Anime, pk=pk)
    if request.method == "POST":
        form = AnimeForm(request.POST, instance=anime)
        if form.is_valid():
            anime = form.save(commit=False)
            anime.save()
            return redirect('anime_detail', pk=anime.pk)
    else:
        form = AnimeForm(instance=anime)
    return render(request, 'registration/mymovies/anime_edit.html', {'form': form})

def anime_delete(request,pk):
    anime = get_object_or_404(Anime, pk=pk)
    delete = anime.delete()
    return render(request, 'registration/mymovies/anime_list.html', {'anime': anime})

# user data management

@login_required

def movie_user_data(request):
    data = Movie.objects.filter(user=request.user)
    return render(request, 'registration/mymovies/movie_list.html', {'data': data})
