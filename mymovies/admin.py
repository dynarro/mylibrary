from django.contrib import admin
from .models import Movie, Serie, Anime


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ('title', 'user',)
    list_filter = ('title', 'genre',)

    fieldsets = (
        (None, {
            'fields': ('title', 'description','image',)
        }),
        ('Availability', {
            'fields': ('user',)
        }),
    )


@admin.register(Serie)
class SerieAdmin(admin.ModelAdmin):
    list_display = ('title', 'user',)
    list_filter = ('title','genre',)

    fieldsets = (
        (None, {
            'fields': ('title', 'description','image',)
        }),
        ('Availability', {
            'fields': ('user',)
        }),
    )


@admin.register(Anime)
class AnimeAdmin(admin.ModelAdmin):
    list_display = ('title', 'user',)
    list_filter = ('title','genre',)

    fieldsets = (
        (None, {
            'fields': ('title', 'description','image',)
        }),
        ('Availability', {
            'fields': ('user',)
        }),
    )
