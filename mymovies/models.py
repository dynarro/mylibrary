from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Image(models.Model):
    image = models.ImageField(null=True, blank=True, upload_to='images/%Y/')

    class Meta:
        abstract = True

class Movie(Image):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    title = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    ranking = models.DecimalField(max_digits=2, decimal_places=1, null=True,blank=True)
    genre = models.CharField(max_length=50)

class Serie(Image):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    title = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    ranking = models.DecimalField(max_digits=2, decimal_places=1, null=True,blank=True)
    genre = models.CharField(max_length=50)


class Anime(Image):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    title = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    ranking = models.DecimalField(max_digits=2, decimal_places=1,
                                  null=True,blank=True)
    genre = models.CharField(max_length=50)
